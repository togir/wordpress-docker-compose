echo "This will create a .env file with default user and random passwords."
echo "Change the .env as you like, you suerly want to change the WEBPORT and FTPPORT"

echo "DBUSER=wp" >> .env
echo "DBPASSWD=$(openssl rand -base64 32)" >> .env

echo "FTPUSER=web" >> .env
echo "FTPPASSWD=$(openssl rand -base64 32)" >> .env

echo "FTPPORT=9001" >> .env
echo "WEBPORT=9000" >> .env
